import { HTML_CONTENT_MIN_LENGTH } from '../src/handler'
import { RequestListener } from '../src/renderer'

export const getResponseWithMinimumSize =
(responseContent: string) => `<!DOCTYPE html>${responseContent}${" ".repeat(HTML_CONTENT_MIN_LENGTH - responseContent.length)}`

export default async function init(): Promise<RequestListener> {
  const cb: RequestListener = (req, res) => {
    if (!req.url) return res.end()
    if (req.url.startsWith('/params')) {
      res.write('params')
    }
    if (req.url === '/hello') {
      res.write(getResponseWithMinimumSize('hello'))
      res.end()
    }  else if (req.url === '/hello-304') {
      res.statusCode = 304
    } else if (req.url === '/hello-empty') {
      res.write('')
    } else {
      res.statusCode = 404
    }
    if (!req.url.startsWith('/slow-')) {
      res.end()
    } else {
      const time = req.url.replace(/\/slow-(\d+)$/, '$1')
      const timeoutMs = parseInt(time, 10) || 100

      console.log(`slow with ${timeoutMs}ms`)
      
      setTimeout(() => {
        res.statusCode = 200
        res.write(getResponseWithMinimumSize(`slow with ${timeoutMs}ms done!`))
        res.end()
      }, timeoutMs)
    }
  }
  return cb
}
