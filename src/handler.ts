import { IncomingMessage } from 'http'

import { lock, send, serveCache, unlock } from './cache-manager'
import { forMetrics, Metrics, serveMetrics } from './metrics'
import { EncodePagePayload, encodePayload } from './payload'
import Renderer, { InitArgs } from './renderer'
import { CacheAdapter, HandlerConfig, WrappedHandler } from './types'
import { filterUrl, getBodyBufferWithUpdatedNonce, getFileExtensionFromUrl, log, mergeConfig, serve } from './utils'

const STATUS_CODES_WITH_CACHE = [200, 308]
const REQUEST_METHODS_WITH_CACHE = ['GET', 'HEAD']

export const GENERAL_CONTENT_MIN_LENGTH = 50
export const HTML_CONTENT_MIN_LENGTH = 5_000

function matchRules(conf: HandlerConfig, req: IncomingMessage) {
  const err = REQUEST_METHODS_WITH_CACHE.indexOf(req.method ?? '') === -1
  if (err) return { matched: false, ttl: -1 }

  if (typeof conf.rules === 'function') {
    const ttl = conf.rules(req)
    if (ttl) return { matched: true, ttl }
  } else {
    for (const rule of conf.rules ?? []) {
      if (req.url && new RegExp(rule.regex).test(req.url)) {
        return { matched: true, ttl: rule.ttl }
      }
    }
  }
  return { matched: false, ttl: 0 }
}

/**
 * all statuses with payload should contain valid htmldoc in body
 * https://github.com/next-boost/next-boost/issues/62#issuecomment-1173380306
 */
const validateHTMLContent = (
  decodedData: string,
): void => {
  const DOCTYPE = '<!DOCTYPE'
  const hasHtml = decodedData.substring(0, DOCTYPE.length).toUpperCase() === DOCTYPE
  const hasMinContent = decodedData.length > HTML_CONTENT_MIN_LENGTH

  if (!hasHtml || !hasMinContent) {
    throw new Error('validateHTMLContent')
  }
}

const validateContentMinLength = (
  decodedData: string,
  minLength: number
): void => {
  const hasMinContent = decodedData.length > minLength

  if (!hasMinContent) {
    throw new Error(`validateContentMinLength, expected=${minLength}, actual=${decodedData?.length}`)
  }
}

/**
 * Wrap a http listener to serve cached response
 *
 * @param cache the cache
 * @param conf conf of next-boost
 * @param renderer the SSR renderer runs in worker thread
 * @param next pass-through handler
 *
 * @returns a request listener to use in http server
 */
const wrap: WrappedHandler = (cache, conf, renderer, next, metrics) => {
  return async (req, res) => {
    if (conf.metrics && forMetrics(req)) return serveMetrics(metrics, res)

    req.url = filterUrl(req.url ?? '', conf.paramFilter)
    const key = conf.cacheKey ? conf.cacheKey(req) : req.url
    const { matched, ttl } = matchRules(conf, req)
    if (!matched) {
      metrics.inc('bypass')
      res.setHeader('x-next-boost-status', 'bypass')
      return next(req, res)
    }

    const start = process.hrtime()
    const forced = req.headers['x-next-boost'] === 'update' // forced
    
    let state = await serveCache(cache, key, forced)
    const requestUrl = req.url
    const fileExtension = getFileExtensionFromUrl(req)

    // these statuses don't have payload
    if (state.status !== 'miss'
      && state.status !== 'timeout'
      && state.status !== 'force'
      && state.status !== 'bypass') {
      try {
        const decodedData = state.payload.body.toString()

        const isRedirectedResponse = state.payload.statusCode >= 300 && state.payload.statusCode < 400
        switch (fileExtension) {
          case "js":
          case "css":
            validateContentMinLength(decodedData, GENERAL_CONTENT_MIN_LENGTH)
            break
          case "json":
            JSON.parse(state.payload.body.toString()) // throws error if invalid
            break
          case "html":
          case null:
            if (!isRedirectedResponse) {
              validateContentMinLength(decodedData, HTML_CONTENT_MIN_LENGTH)
              validateHTMLContent(decodedData)
            }
            break
          default:
            console.warn(`Unknown file extension in next-boost, url='${requestUrl}'`, requestUrl)
            break
        }
      } catch (e) {
        console.error(`Failed to validate buffer, status='${state.status}', statusCode='${state.payload.statusCode}' url='${requestUrl}', decodedData='${state.payload.body.toString().substring(0, 100)}...'`, e)
        state = {
          status: 'force',
        }
      }
    }
    res.setHeader('x-next-boost-status', state.status)
    metrics.inc(state.status)

    if (state.status === 'stale' || state.status === 'hit' || state.status === 'fulfill') {
      send({
        ...state.payload,
        body: getBodyBufferWithUpdatedNonce(req, state.payload.body),
      }, res)
      if (!conf.quiet) log(start, state.status, req.url) // record time for stale and hit
      if (state.status !== 'stale') return // stop here
    } else if (state.status === 'timeout') {
      send({ body: null, headers: null }, res)
      return // prevent adding pressure to server
    }

    try {
      await lock(key, cache)

      const args = { path: req.url, headers: req.headers, method: req.method }
      const rv = await renderer.render(args)
      // rv.body is a Buffer in JSON format: { type: 'Buffer', data: [...] }
      const body = Buffer.from(rv.body)
      // stale has been served
      if (state.status !== 'stale') serve(req, res, rv)
      // when in stale, there will 2 log output. The latter is the rendering time on server
      if (!conf.quiet) log(start, state.status, req.url)
      if (STATUS_CODES_WITH_CACHE.includes(rv.statusCode)) {
        const payload: EncodePagePayload = {
          headers: rv.headers,
          body,
          statusCode: rv.statusCode
        }
        await cache.set(
          'payload:' + key,
          encodePayload(payload),
          ttl
        )
      } else {
        // https://github.com/next-boost/next-boost/issues/74
        cache.del('payload:' + key)
      }
    } catch (e) {
      console.error('Error saving payload to cache', e)
    } finally {
      await unlock(key, cache)
    }
  }
}

export default async function CachedHandler(args: InitArgs, options?: HandlerConfig) {
  console.log('> Preparing cached handler')

  // merge config
  const conf = mergeConfig(options)

  // the cache
  if (!conf.cacheAdapter) {
    const { Adapter } = require('@next-boost/hybrid-disk-cache')
    conf.cacheAdapter = new Adapter() as CacheAdapter
  }
  const adapter = conf.cacheAdapter
  const cache = await adapter.init()

  const renderer = Renderer()
  await renderer.init(args)
  const plain = await require(args.script).default(args.args)

  const metrics = new Metrics()

  // init the child process for revalidate and cache purge
  return {
    handler: wrap(cache, conf, renderer, plain, metrics),
    cache,
    close: async () => {
      renderer.kill()
      await adapter.shutdown()
    },
  }
}
