import { ServerResponse } from 'http'
import { PassThrough } from 'stream'

import { decodePayload } from './payload'
import { Cache, State } from './types'
import { sleep } from './utils'

const MAX_WAIT_MS = 20_000
const CACHE_RENDER_LOCK_SECONDS = MAX_WAIT_MS / 1_000
const LOCK_WAIT_INTERVAL_MS = 10

export async function hasLock(key: string, cache: Cache) {
  return (await cache.has('lock:' + key)) === 'hit'
}

// mutex lock to prevent same page rendered more than once
export async function lock(key: string, cache: Cache) {
  await cache.set('lock:' + key, Buffer.from('lock'), CACHE_RENDER_LOCK_SECONDS)
}

export async function unlock(key: string, cache: Cache) {
  await cache.del('lock:' + key)
}

export async function serveCache(cache: Cache, key: string, forced: boolean): Promise<State> {
  if (forced) return { status: 'force' }

  try {
    const status = await cache.has('payload:' + key)
    if (status === 'hit') {
      const payload = decodePayload(await cache.get('payload:' + key))
      return { status: 'hit', payload }
    } else if (status === 'miss') {
      const lock = await hasLock(key, cache)
      // non first-time miss (the cache is being created), wait for the cache
      return !lock ? { status: 'miss' } : waitAndServe(key, cache)
    } else {
      // stale
      const payload = decodePayload(await cache.get('payload:' + key))
      return { status: 'stale', payload }
    }
  } catch (e) {
    console.error(`${key} cache error`, e)
    return { status: 'miss' }
  }
}

async function waitAndServe(key: string, cache: Cache): Promise<State> {
  while (await hasLock(key, cache)) {
    // lock will expire
    await sleep(LOCK_WAIT_INTERVAL_MS)
  }
  const status = await cache.has('payload:' + key)
  // still no cache after waiting for MAX_WAIT
  if (status === 'miss') {
    return { status: 'timeout' }
  } else {
    const payload = decodePayload(await cache.get('payload:' + key))
    return { status: 'fulfill', payload }
  }
}

export function send(
  payload: { body: Buffer | null; headers: Record<string, any> | null; statusCode?: number },
  res: ServerResponse,
) {
  const { body, headers, statusCode } = payload
  if (!body || !statusCode) {
    res.statusCode = 504
    return res.end()
  }
  for (const k in headers) {
    res.setHeader(k, headers[k])
  }
  res.statusCode = statusCode
  res.removeHeader('transfer-encoding')
  res.setHeader('content-length', Buffer.byteLength(body))
  const stream = new PassThrough()
  stream.pipe(res)
  stream.end(body)
}
