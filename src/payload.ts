import type { RenderResult } from "./renderer"

export type DecodedPagePayload = {
  headers: RenderResult["headers"]
  body: Buffer
  statusCode: number
}
export type EncodePagePayload = {
  headers: RenderResult["headers"] | null
  body: Buffer | null
  statusCode: number
}

const MAGIC = Buffer.from('%NB%')
const LENGTH_SIZE = 4
const STATUS_CODE_SIZE = 3

/**
 * Package the headers and body of a page to a binary format.
 * The format will be `%NB%` + length of the headers + headers + body + statusCode.
 *
 * @param payload The page payload.
 * @returns The binary payload.
 */
export function encodePayload({ headers, body, statusCode = 200 }: EncodePagePayload): Buffer {
  const headerBuffer = Buffer.from(JSON.stringify(headers))
  const headerLength = Buffer.alloc(LENGTH_SIZE)
  headerLength.writeUInt32BE(headerBuffer.length, 0)
  return Buffer.concat([
    MAGIC,
    headerLength,
    headerBuffer,
    body ? body : Buffer.alloc(0),
    Buffer.from(statusCode.toString())
  ])
}

/**
 * Read the headers and body of a page from a binary payload.
 *
 * @param payload The binary payload.
 * @returns The page payload.
 */
export function decodePayload(payload: Buffer | undefined): DecodedPagePayload {
  if (!payload) {
    return {
      headers: {},
      body: Buffer.alloc(0),
      statusCode: 200
    }
  }

  const magic = payload.subarray(0, MAGIC.length)
  if (MAGIC.compare(magic) !== 0) {
    throw new Error('Invalid payload')
  }

  const headerLength = payload.readUInt32BE(MAGIC.length)

  const headerBufferStart = MAGIC.length + LENGTH_SIZE
  const headerBufferEnd = headerBufferStart + headerLength
  const headerBuffer = payload.subarray(headerBufferStart, headerBufferEnd)

  const bodyBufferEnd = payload.length - STATUS_CODE_SIZE
  const bodyBuffer = payload.subarray(headerBufferEnd, bodyBufferEnd)

  const statusCodeBuffer = payload.subarray(bodyBufferEnd)

  return {
    headers: JSON.parse(headerBuffer.toString()),
    body: bodyBuffer,
    statusCode: statusCodeBuffer && !Number.isNaN(Number(statusCodeBuffer))
      ? Number(statusCodeBuffer)
      : 200,
  }
}
