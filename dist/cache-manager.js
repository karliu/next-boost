"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
function _export(target, all) {
    for(var name in all)Object.defineProperty(target, name, {
        enumerable: true,
        get: all[name]
    });
}
_export(exports, {
    hasLock: function() {
        return hasLock;
    },
    lock: function() {
        return lock;
    },
    unlock: function() {
        return unlock;
    },
    serveCache: function() {
        return serveCache;
    },
    send: function() {
        return send;
    }
});
const _stream = require("stream");
const _payload = require("./payload");
const _utils = require("./utils");
const MAX_WAIT_MS = 20000;
const CACHE_RENDER_LOCK_SECONDS = MAX_WAIT_MS / 1000;
const LOCK_WAIT_INTERVAL_MS = 10;
async function hasLock(key, cache) {
    return await cache.has('lock:' + key) === 'hit';
}
async function lock(key, cache) {
    await cache.set('lock:' + key, Buffer.from('lock'), CACHE_RENDER_LOCK_SECONDS);
}
async function unlock(key, cache) {
    await cache.del('lock:' + key);
}
async function serveCache(cache, key, forced) {
    if (forced) return {
        status: 'force'
    };
    try {
        const status = await cache.has('payload:' + key);
        if (status === 'hit') {
            const payload = (0, _payload.decodePayload)(await cache.get('payload:' + key));
            return {
                status: 'hit',
                payload
            };
        } else if (status === 'miss') {
            const lock = await hasLock(key, cache);
            // non first-time miss (the cache is being created), wait for the cache
            return !lock ? {
                status: 'miss'
            } : waitAndServe(key, cache);
        } else {
            // stale
            const payload = (0, _payload.decodePayload)(await cache.get('payload:' + key));
            return {
                status: 'stale',
                payload
            };
        }
    } catch (e) {
        console.error(`${key} cache error`, e);
        return {
            status: 'miss'
        };
    }
}
async function waitAndServe(key, cache) {
    while(await hasLock(key, cache)){
        // lock will expire
        await (0, _utils.sleep)(LOCK_WAIT_INTERVAL_MS);
    }
    const status = await cache.has('payload:' + key);
    // still no cache after waiting for MAX_WAIT
    if (status === 'miss') {
        return {
            status: 'timeout'
        };
    } else {
        const payload = (0, _payload.decodePayload)(await cache.get('payload:' + key));
        return {
            status: 'fulfill',
            payload
        };
    }
}
function send(payload, res) {
    const { body , headers , statusCode  } = payload;
    if (!body || !statusCode) {
        res.statusCode = 504;
        return res.end();
    }
    for(const k in headers){
        res.setHeader(k, headers[k]);
    }
    res.statusCode = statusCode;
    res.removeHeader('transfer-encoding');
    res.setHeader('content-length', Buffer.byteLength(body));
    const stream = new _stream.PassThrough();
    stream.pipe(res);
    stream.end(body);
}
