"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
function _export(target, all) {
    for(var name in all)Object.defineProperty(target, name, {
        enumerable: true,
        get: all[name]
    });
}
_export(exports, {
    serveMetrics: function() {
        return serveMetrics;
    },
    forMetrics: function() {
        return forMetrics;
    },
    Metrics: function() {
        return Metrics;
    }
});
async function serveMetrics(m, res) {
    res.setHeader('content-type', 'text/plain; version=0.0.4');
    Object.keys(m.data).forEach((k)=>{
        res.write(`next_boost_requests_total{status='${k}'} ${m.data[k]}\n`);
    });
    res.end();
}
function forMetrics(req) {
    return req.url === '/__nextboost_metrics';
}
let Metrics = class Metrics {
    inc(key) {
        return this.data[key] = (this.data[key] || 0) + 1;
    }
    constructor(){
        this.data = {};
    }
};
