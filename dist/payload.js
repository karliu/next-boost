"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
function _export(target, all) {
    for(var name in all)Object.defineProperty(target, name, {
        enumerable: true,
        get: all[name]
    });
}
_export(exports, {
    encodePayload: function() {
        return encodePayload;
    },
    decodePayload: function() {
        return decodePayload;
    }
});
const MAGIC = Buffer.from('%NB%');
const LENGTH_SIZE = 4;
const STATUS_CODE_SIZE = 3;
function encodePayload({ headers , body , statusCode =200  }) {
    const headerBuffer = Buffer.from(JSON.stringify(headers));
    const headerLength = Buffer.alloc(LENGTH_SIZE);
    headerLength.writeUInt32BE(headerBuffer.length, 0);
    return Buffer.concat([
        MAGIC,
        headerLength,
        headerBuffer,
        body ? body : Buffer.alloc(0),
        Buffer.from(statusCode.toString())
    ]);
}
function decodePayload(payload) {
    if (!payload) {
        return {
            headers: {},
            body: Buffer.alloc(0),
            statusCode: 200
        };
    }
    const magic = payload.subarray(0, MAGIC.length);
    if (MAGIC.compare(magic) !== 0) {
        throw new Error('Invalid payload');
    }
    const headerLength = payload.readUInt32BE(MAGIC.length);
    const headerBufferStart = MAGIC.length + LENGTH_SIZE;
    const headerBufferEnd = headerBufferStart + headerLength;
    const headerBuffer = payload.subarray(headerBufferStart, headerBufferEnd);
    const bodyBufferEnd = payload.length - STATUS_CODE_SIZE;
    const bodyBuffer = payload.subarray(headerBufferEnd, bodyBufferEnd);
    const statusCodeBuffer = payload.subarray(bodyBufferEnd);
    return {
        headers: JSON.parse(headerBuffer.toString()),
        body: bodyBuffer,
        statusCode: statusCodeBuffer && !Number.isNaN(Number(statusCodeBuffer)) ? Number(statusCodeBuffer) : 200
    };
}
