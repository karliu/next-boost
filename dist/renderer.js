"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
Object.defineProperty(exports, "default", {
    enumerable: true,
    get: function() {
        return _default;
    }
});
const _http = /*#__PURE__*/ _interop_require_default(require("http"));
const _multee = /*#__PURE__*/ _interop_require_default(require("multee"));
function _interop_require_default(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
const { createHandler , start  } = (0, _multee.default)('worker');
let server;
const init = createHandler('init', async (args)=>{
    if (!args) throw new Error('args is required');
    const fn = require(args.script).default;
    server = new _http.default.Server(await fn(args.args)).listen(0);
});
const render = createHandler('renderer', async (options)=>{
    return new Promise((resolve, reject)=>{
        const addr = server.address();
        if (typeof addr !== 'object' || !addr) {
            return reject('Failed to create server in renderer');
        }
        const args = {
            hostname: '127.0.0.1',
            port: addr.port,
            ...options
        };
        const req = _http.default.request(args, (res)=>{
            let body = Buffer.from('');
            res.on('data', (chunk)=>body = Buffer.concat([
                    body,
                    chunk
                ]));
            var _res_statusCode;
            res.on('end', ()=>resolve({
                    headers: res.headers,
                    statusCode: (_res_statusCode = res.statusCode) != null ? _res_statusCode : 200,
                    body
                }));
        });
        req.on('error', (e)=>reject(`Failed in renderer: ${e.message}`));
        req.end();
    });
});
const _default = ()=>{
    const child = start(__filename);
    return {
        init: init(child),
        render: render(child),
        kill: ()=>child.terminate()
    };
};
