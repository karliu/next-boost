"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
Object.defineProperty(exports, "default", {
    enumerable: true,
    get: function() {
        return init;
    }
});
async function init(args) {
    const app = require('next')(args);
    await app.prepare();
    return app.getRequestHandler();
}
