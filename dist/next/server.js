#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
const _http = /*#__PURE__*/ _interop_require_default(require("http"));
const _httpgracefulshutdown = /*#__PURE__*/ _interop_require_default(require("http-graceful-shutdown"));
const _cli = require("../cli");
const _handler = /*#__PURE__*/ _interop_require_default(require("../handler"));
function _interop_require_default(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
process.env.NODE_ENV = 'production';
const serve = async (argv)=>{
    const port = argv['--port'] || 3000;
    // no host binding by default, the same as `next start`
    const hostname = argv['--hostname'];
    const quiet = argv['--quiet'];
    const dir = argv['dir'] || '.';
    const grace = argv['--grace'] || 30000;
    const script = require.resolve('./init');
    const rendererArgs = {
        script,
        args: {
            dir,
            dev: false,
            hostname,
            port
        }
    };
    const cached = await (0, _handler.default)(rendererArgs, {
        quiet
    });
    const server = new _http.default.Server(cached.handler);
    server.listen(port, hostname, ()=>{
        console.log(`> Serving on http://${hostname || 'localhost'}:${port}`);
    });
    (0, _httpgracefulshutdown.default)(server, {
        timeout: grace,
        preShutdown: async ()=>console.log('> Shutting down...'),
        onShutdown: async ()=>cached.close(),
        finally: ()=>console.log('> Shutdown complete.')
    });
};
if (require.main === module) {
    const argv = (0, _cli.parse)(process.argv);
    if (argv) serve(argv);
}
