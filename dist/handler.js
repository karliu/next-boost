"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
function _export(target, all) {
    for(var name in all)Object.defineProperty(target, name, {
        enumerable: true,
        get: all[name]
    });
}
_export(exports, {
    GENERAL_CONTENT_MIN_LENGTH: function() {
        return GENERAL_CONTENT_MIN_LENGTH;
    },
    HTML_CONTENT_MIN_LENGTH: function() {
        return HTML_CONTENT_MIN_LENGTH;
    },
    default: function() {
        return CachedHandler;
    }
});
const _cachemanager = require("./cache-manager");
const _metrics = require("./metrics");
const _payload = require("./payload");
const _renderer = /*#__PURE__*/ _interop_require_default(require("./renderer"));
const _utils = require("./utils");
function _interop_require_default(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
const STATUS_CODES_WITH_CACHE = [
    200,
    308
];
const REQUEST_METHODS_WITH_CACHE = [
    'GET',
    'HEAD'
];
const GENERAL_CONTENT_MIN_LENGTH = 50;
const HTML_CONTENT_MIN_LENGTH = 5000;
function matchRules(conf, req) {
    var _req_method;
    const err = REQUEST_METHODS_WITH_CACHE.indexOf((_req_method = req.method) != null ? _req_method : '') === -1;
    if (err) return {
        matched: false,
        ttl: -1
    };
    if (typeof conf.rules === 'function') {
        const ttl = conf.rules(req);
        if (ttl) return {
            matched: true,
            ttl
        };
    } else {
        var _conf_rules;
        for (const rule of (_conf_rules = conf.rules) != null ? _conf_rules : []){
            if (req.url && new RegExp(rule.regex).test(req.url)) {
                return {
                    matched: true,
                    ttl: rule.ttl
                };
            }
        }
    }
    return {
        matched: false,
        ttl: 0
    };
}
/**
 * all statuses with payload should contain valid htmldoc in body
 * https://github.com/next-boost/next-boost/issues/62#issuecomment-1173380306
 */ const validateHTMLContent = (decodedData)=>{
    const DOCTYPE = '<!DOCTYPE';
    const hasHtml = decodedData.substring(0, DOCTYPE.length).toUpperCase() === DOCTYPE;
    const hasMinContent = decodedData.length > HTML_CONTENT_MIN_LENGTH;
    if (!hasHtml || !hasMinContent) {
        throw new Error('validateHTMLContent');
    }
};
const validateContentMinLength = (decodedData, minLength)=>{
    const hasMinContent = decodedData.length > minLength;
    if (!hasMinContent) {
        throw new Error(`validateContentMinLength, expected=${minLength}, actual=${decodedData == null ? void 0 : decodedData.length}`);
    }
};
/**
 * Wrap a http listener to serve cached response
 *
 * @param cache the cache
 * @param conf conf of next-boost
 * @param renderer the SSR renderer runs in worker thread
 * @param next pass-through handler
 *
 * @returns a request listener to use in http server
 */ const wrap = (cache, conf, renderer, next, metrics)=>{
    return async (req, res)=>{
        if (conf.metrics && (0, _metrics.forMetrics)(req)) return (0, _metrics.serveMetrics)(metrics, res);
        var _req_url;
        req.url = (0, _utils.filterUrl)((_req_url = req.url) != null ? _req_url : '', conf.paramFilter);
        const key = conf.cacheKey ? conf.cacheKey(req) : req.url;
        const { matched , ttl  } = matchRules(conf, req);
        if (!matched) {
            metrics.inc('bypass');
            res.setHeader('x-next-boost-status', 'bypass');
            return next(req, res);
        }
        const start = process.hrtime();
        const forced = req.headers['x-next-boost'] === 'update' // forced
        ;
        let state = await (0, _cachemanager.serveCache)(cache, key, forced);
        const requestUrl = req.url;
        const fileExtension = (0, _utils.getFileExtensionFromUrl)(req);
        // these statuses don't have payload
        if (state.status !== 'miss' && state.status !== 'timeout' && state.status !== 'force' && state.status !== 'bypass') {
            try {
                const decodedData = state.payload.body.toString();
                const isRedirectedResponse = state.payload.statusCode >= 300 && state.payload.statusCode < 400;
                switch(fileExtension){
                    case "js":
                    case "css":
                        validateContentMinLength(decodedData, GENERAL_CONTENT_MIN_LENGTH);
                        break;
                    case "json":
                        JSON.parse(state.payload.body.toString()) // throws error if invalid
                        ;
                        break;
                    case "html":
                    case null:
                        if (!isRedirectedResponse) {
                            validateContentMinLength(decodedData, HTML_CONTENT_MIN_LENGTH);
                            validateHTMLContent(decodedData);
                        }
                        break;
                    default:
                        console.warn(`Unknown file extension in next-boost, url='${requestUrl}'`, requestUrl);
                        break;
                }
            } catch (e) {
                console.error(`Failed to validate buffer, status='${state.status}', statusCode='${state.payload.statusCode}' url='${requestUrl}', decodedData='${state.payload.body.toString().substring(0, 100)}...'`, e);
                state = {
                    status: 'force'
                };
            }
        }
        res.setHeader('x-next-boost-status', state.status);
        metrics.inc(state.status);
        if (state.status === 'stale' || state.status === 'hit' || state.status === 'fulfill') {
            (0, _cachemanager.send)({
                ...state.payload,
                body: (0, _utils.getBodyBufferWithUpdatedNonce)(req, state.payload.body)
            }, res);
            if (!conf.quiet) (0, _utils.log)(start, state.status, req.url) // record time for stale and hit
            ;
            if (state.status !== 'stale') return; // stop here
        } else if (state.status === 'timeout') {
            (0, _cachemanager.send)({
                body: null,
                headers: null
            }, res);
            return; // prevent adding pressure to server
        }
        try {
            await (0, _cachemanager.lock)(key, cache);
            const args = {
                path: req.url,
                headers: req.headers,
                method: req.method
            };
            const rv = await renderer.render(args);
            // rv.body is a Buffer in JSON format: { type: 'Buffer', data: [...] }
            const body = Buffer.from(rv.body);
            // stale has been served
            if (state.status !== 'stale') (0, _utils.serve)(req, res, rv);
            // when in stale, there will 2 log output. The latter is the rendering time on server
            if (!conf.quiet) (0, _utils.log)(start, state.status, req.url);
            if (STATUS_CODES_WITH_CACHE.includes(rv.statusCode)) {
                const payload = {
                    headers: rv.headers,
                    body,
                    statusCode: rv.statusCode
                };
                await cache.set('payload:' + key, (0, _payload.encodePayload)(payload), ttl);
            } else {
                // https://github.com/next-boost/next-boost/issues/74
                cache.del('payload:' + key);
            }
        } catch (e) {
            console.error('Error saving payload to cache', e);
        } finally{
            await (0, _cachemanager.unlock)(key, cache);
        }
    };
};
async function CachedHandler(args, options) {
    console.log('> Preparing cached handler');
    // merge config
    const conf = (0, _utils.mergeConfig)(options);
    // the cache
    if (!conf.cacheAdapter) {
        const { Adapter  } = require('@next-boost/hybrid-disk-cache');
        conf.cacheAdapter = new Adapter();
    }
    const adapter = conf.cacheAdapter;
    const cache = await adapter.init();
    const renderer = (0, _renderer.default)();
    await renderer.init(args);
    const plain = await require(args.script).default(args.args);
    const metrics = new _metrics.Metrics();
    // init the child process for revalidate and cache purge
    return {
        handler: wrap(cache, conf, renderer, plain, metrics),
        cache,
        close: async ()=>{
            renderer.kill();
            await adapter.shutdown();
        }
    };
}
