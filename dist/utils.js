"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
function _export(target, all) {
    for(var name in all)Object.defineProperty(target, name, {
        enumerable: true,
        get: all[name]
    });
}
_export(exports, {
    log: function() {
        return log;
    },
    mergeConfig: function() {
        return mergeConfig;
    },
    sleep: function() {
        return sleep;
    },
    serve: function() {
        return serve;
    },
    filterUrl: function() {
        return filterUrl;
    },
    getBodyBufferWithUpdatedNonce: function() {
        return getBodyBufferWithUpdatedNonce;
    },
    getFileExtensionFromUrl: function() {
        return getFileExtensionFromUrl;
    }
});
const _fs = /*#__PURE__*/ _interop_require_default(require("fs"));
const _path = /*#__PURE__*/ _interop_require_default(require("path"));
function _interop_require_default(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
const NONCE_WILDCARD = "$CACHED_NONCE";
function log(start, status, msg) {
    const [secs, ns] = process.hrtime(start);
    const ms = ns / 1000000;
    const timeS = `${secs > 0 ? secs + 's' : ''}`;
    const timeMs = `${secs === 0 ? ms.toFixed(1) : ms.toFixed(0)}ms`;
    const time = timeS + (secs > 1 ? '' : timeMs);
    console.log('%s | %s: %s', time.padStart(7), status.padEnd(6), msg);
}
function serve(req, res, rv) {
    for(const k in rv.headers)res.setHeader(k, rv.headers[k]);
    res.statusCode = rv.statusCode;
    const body = getBodyBufferWithUpdatedNonce(req, Buffer.from(rv.body));
    res.end(body);
}
function mergeConfig(c = {}) {
    const conf = {
        rules: [
            {
                regex: '.*',
                ttl: 3600
            }
        ]
    };
    if (!c.filename) c.filename = '.next-boost.js';
    const configFile = _path.default.resolve(c.filename);
    if (_fs.default.existsSync(configFile)) {
        try {
            const f = require(configFile);
            c.quiet = c.quiet || f.quiet;
            c = Object.assign(f, c);
            console.log('  Loaded next-boost config from %s', c.filename);
        } catch (error) {
            throw new Error(`Failed to load ${c.filename}`);
        }
    }
    Object.assign(conf, c);
    return conf;
}
function filterUrl(url, filter) {
    if (!filter) return url;
    const [p0, p1] = url.split('?', 2);
    const params = new URLSearchParams(p1);
    const keysToDelete = [
        ...params.keys()
    ].filter((k)=>!filter(k));
    for (const k of keysToDelete)params.delete(k);
    const qs = params.toString();
    return qs ? p0 + '?' + qs : p0;
}
async function sleep(ms) {
    return new Promise((resolve)=>setTimeout(resolve, ms));
}
function getBodyBufferWithUpdatedNonce(req, _body) {
    let body = _body;
    try {
        let htmlContent = body.toString('utf-8');
        // @ts-expect-error global express types
        const nonce = req.nonce;
        const fileExtension = getFileExtensionFromUrl(req);
        const isNonceNeededForPath = !fileExtension || fileExtension === "html";
        if (typeof nonce === "string" && isNonceNeededForPath) {
            htmlContent = htmlContent.replace(NONCE_WILDCARD, nonce);
            body = Buffer.from(htmlContent, 'utf-8');
        }
    } catch (e) {
        console.error("Failed to update nonce in body", e);
    }
    return body;
}
function getFileExtensionFromUrl(req) {
    var _exec;
    const requestUrl = req.url;
    const { pathname: requestPathname  } = new URL(`http://localhost/${requestUrl}`);
    var _exec_;
    return (_exec_ = (_exec = /\.([^.\\?]+)$/.exec(requestPathname)) == null ? void 0 : _exec[1]) != null ? _exec_ : null;
}
